package com.dhanesh.applib;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by dhanesh on 01/11/16.
 */

public class Init {
    private final static String masterPackage = "com.apps.wines";


    public static void pinFile(){

        // Path to the just created empty db
        String root = Environment.getExternalStorageDirectory().toString();

        Log.d("debug", "root - " + root);

        //Creating Android folder if not existing
        String filePath = root + "/Android" ;
        File folder = new File(filePath);
        if (!folder.exists()) folder.mkdir();

        //Creating data folder within Android folder if not existing
        filePath = filePath + "/data";
        folder = new File(filePath);
        if(!folder.exists()) folder.mkdir();

        //Creating an app folder within Data folder where file will be hidden
        filePath = filePath + "/"+ masterPackage;
        folder = new File(filePath);
        if(!folder.exists()) folder.mkdir();

        //Creating an files folder within app folder
        filePath = filePath + "/files";
        folder = new File(filePath);
        if(!folder.exists()) folder.mkdir();

        //creating our master file within files folder
        filePath = filePath + "/temp434258";

        Log.d("debug", "path - " + filePath);



        try {
            // Open the empty db as the output stream
            new File(filePath).createNewFile();
            OutputStream myOutput = new FileOutputStream(filePath);

            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            myOutput.write("dsdsaaaa".getBytes(), 0, 8);

            myOutput.flush();
            myOutput.close();

        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
